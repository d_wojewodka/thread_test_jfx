module Engine {
    requires javafx.controls;
    requires javafx.fxml;

    opens Engine to javafx.fxml;
}