import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    private static Scene scene;
    private static Stage stage;

    @Override public void start(Stage st) throws IOException {
        scene = new Scene(loadFXML());
        st.setScene(scene);

        stage = st;
        stage.setTitle("Engine controlller");
        stage.setResizable(false);
        stage.show();
    }

    private static Parent loadFXML() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("guiController.fxml"));
        Parent root = fxmlLoader.load();
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}