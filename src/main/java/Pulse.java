public class Pulse extends Thread {
    private Integer pulse_interval, pause_time;
    private boolean shutdown;
    private int loop;
    private Long last_pulse_time;

    Pulse(Integer time_ms) {
        initialize(time_ms);
    }

    public void initialize(Integer time_ms) {
        pulse_interval = time_ms;
        pause_time = 0;
        shutdown = false;
        loop = 1;
        last_pulse_time = System.currentTimeMillis();
    }

    public void changeInterval(Integer time_ms) {
        pulse_interval = time_ms;
    }

    public void stopPulse() {
        shutdown = true;
    }
    public void pausePulse(Integer time_ms) {
        System.out.printf("[PULSE_PAUSE] STARTED FOR %.3f seconds%n", time_ms/1000.0);
        pause_time = time_ms;
    }

    @Override
    public void run() { // core pulse [value: miliseconds]
        if (shutdown) {
            System.out.println("Pulse is going off");
            shutdown = true;
        }

        while (!shutdown) {
            if (pause_time == 0) {
                // put your code here
                String log = String.format("[CORE_PULSE] Refresh rate: %dms, loop: %d, last_pulse_time: %.3f seconds", pulse_interval, loop, (System.currentTimeMillis() - last_pulse_time)/1000.0);
                //System.out.println(log);
                Log.append(log);
            }

            try {
                last_pulse_time = System.currentTimeMillis();

                if (pause_time > 0) {
                    Thread.sleep(pause_time > 0 ? pause_time : pulse_interval);
                    pause_time = 0;
                    System.out.println("[PULSE_PAUSE] FINISHED");
                }
                else {
                    Thread.sleep(pulse_interval);
                }
                loop++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
