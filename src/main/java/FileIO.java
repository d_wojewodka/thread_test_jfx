import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;


public class FileIO extends File  {
    private String sPath;
    private String sFileName;
    private String sExtension;

    private File fFile = null;
    private Charset chCharset = null;

    public FileIO(String pathname) throws IOException {
        super(pathname);
        loadFile(pathname);
    }

    private void Initialize() {
        this.sPath = "";
        this.sFileName = "";
        this.sExtension = "";
    }

    public void loadFile(String custom) throws IOException {
        setFile(new File(custom));
        setCharset(Charset.defaultCharset());

        if (!fFile.exists()) { //create file if class loaded
            fFile.createNewFile();
        }
    }

    public void setFile(File file) throws IOException {
        if (!file.equals(fFile)) {
            Initialize();

            this.fFile = file;
            reloadFile();
        }
    }
    public void setCharset(Charset charset) { this.chCharset = charset; }

    public File getFile() { return this.fFile; }
    public String getFilePath() { return this.sPath; }
    public String getFileName() { return this.sFileName; }
    public String getFileExtension() { return this.sExtension; }
    public Charset getCharset() { return this.chCharset; }

    public boolean renameFile(String name, String extension) throws IOException {
        File new_file = new File(name + "." + extension);
        if (!new_file.exists()) {
            this.sFileName = name;
            this.sExtension = extension;
            this.sPath = new_file.getAbsolutePath();

            this.fFile.renameTo(new_file);
            setFile(new_file);

            return true;
        }
        else {
            System.err.printf("Cannot rename file %s.%s to %s.%s because it is already exists!%n", sFileName, sExtension, name, extension);
            return false;
        }
    }

    private void reloadFile() {
        this.sPath = this.fFile.getAbsolutePath();

        String name = Path.of(this.sPath).getFileName().toString();

        if (name.contains(".")) {
            this.sFileName = name.substring(0, name.indexOf("."));
            this.sExtension = name.substring(name.indexOf(".")+1);
        }
        else {
            this.sFileName = name;
        }
    }



    private SortedMap<Integer, String> sm_FileContent;

    public void loadFileContent() {
        sm_FileContent = new TreeMap();

        if (fFile.exists()) {
            try (BufferedReader reader = Files.newBufferedReader(this.fFile.toPath(), this.chCharset)) {
                String line_content;
                Integer line_index = 1;

                while ((line_content = reader.readLine()) != null) {
                    sm_FileContent.put(line_index, line_content);
                    line_index++;
                }
            } catch (IOException ex) {
                System.err.format("loadFileContent -> IOException: %s%n", ex);
                return;
            }

            //System.out.printf("%s> %s%n", sFileName, sm_FileContent);
        }
    }
    public SortedMap<Integer, String> getFileContent() {
        loadFileContent();
        return this.sm_FileContent;
    }
    public boolean copyFileContentTo(File file, StandardOpenOption... oo) {
        if (file.equals(this.fFile)) {
            System.err.println("You cannot copy to the same file. (It's pointless)");
            return false;
        }
        else if (file.exists()) {
            String file_content = "";

            for (int i=1; i<=this.sm_FileContent.size(); i++) {
                file_content += i == this.sm_FileContent.size() ? this.sm_FileContent.get(i) : this.sm_FileContent.get(i) + System.lineSeparator();
            }

            try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(file.toPath(), oo))) {
                byte bContent[] = file_content.getBytes(this.chCharset);

                out.write(bContent, 0, bContent.length);
                return true;
            }
            catch (IOException ex) {
                System.err.println("file does not exists " + ex);
                return false;
            }
        }
        else {
            System.err.println("copyFileContentTo false; -> file does not exists!");
            return false;
        }
    }
    public String fileContentAsString() {
        String content = "";

        for (int i=1; i<=sm_FileContent.size(); i++) {
            content += i == sm_FileContent.size() ? sm_FileContent.get(i) : sm_FileContent.get(i) + System.lineSeparator();
        }

        return content;
    }
    public boolean writeToFile(String content, StandardOpenOption... oo) {
        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(this.fFile.toPath(), oo))) {
            List lista = Arrays.asList(oo);

            content = !lista.contains(StandardOpenOption.WRITE) ? System.lineSeparator() + content : content;
            out.write(content.getBytes(), 0, content.getBytes().length);

            return true;
        }
        catch (IOException ex) {
            System.out.println("file does not exists " + ex);
            return false;
        }
    }

    public Integer getLineCount() {
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(this.fFile));
            try {
                byte[] c = new byte[1024];

                int readChars = is.read(c);
                if (readChars == -1) {
                    // bail out if nothing to read
                    return 0;
                }

                // make it easy for the optimizer to tune this loop
                int count = 0;
                while (readChars == 1024) {
                    for (int i = 0; i < 1024; ) {
                        if (c[i++] == '\n') {
                            ++count;
                        }
                    }
                    readChars = is.read(c);
                }

                // count remaining characters
                while (readChars != -1) {
                    for (int i = 0; i < readChars; ++i) {
                        if (c[i] == '\n') {
                            ++count;
                        }
                    }
                    readChars = is.read(c);
                }

                return count == 0 ? 1 : count;
            }
            finally {
                is.close();
            }
        }
        catch (FileNotFoundException ex) {
            System.err.format("getLineCount -> FileNotFoundException: %s%n", ex);
        }
        catch (IOException ex) {
            System.err.format("getLineCount -> IOException: %s%n", ex);
        }

        return 0;
    }
}