import java.util.Locale;
import java.util.Scanner;

public class Core {
    public final Integer core_pulse_interval = 250; // refresh rate ms

    private Long start_time;
    private Pulse refPulse;
    private Log log;

    public void pause(Integer time_ms) {
        // code before pause time started
        System.out.printf("[PAUSE] PAUSED FOR %.3f seconds%n", time_ms/1000.0);

        try {
            Thread.sleep(time_ms); // pause for main core
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // code after pause time ends
        System.out.printf("[PAUSE] FINISHED PAUSE%n");
    }

    private void initialize() {
        start_time = System.currentTimeMillis();
        refPulse = new Pulse(core_pulse_interval);
        log = new Log();
        //refPulse = null;
    }

    public void start() {
        if (start_time != null) {
            System.err.println("CORE_IS_ALREADY_RUNNING");
            return;
        }

        // initialize
        initialize();

        menuLoop();

        /*
        // create Pulse
        refPulse = new Pulse(core_pulse_interval);  //pulse_core = new Thread(pulse);
        // start Pulse
        refPulse.start();

        // pause core & pulse
        //pause(2000);

        refPulse.pausePulse(2500);
        refPulse.pausePulse(1);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        refPulse.stopPulse();
        System.out.println("pulse stopped");

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.printf("Core is beeing shut down%n");
        System.out.printf("--- Core was running %.3f seconds ---%n", (System.currentTimeMillis() - start_time)/1000.0);
        */
    }




    public void menuLoop() {
        Scanner input = new Scanner(System.in);
        Scanner arg = new Scanner(System.in);
        System.out.println("MenuLoop, type help for command list");

        while (true) {
            try {
                Thread.sleep(50);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.printf("enter command: ");
            String select = input.nextLine().toLowerCase();

            if (select.equals("help")) {
                help();
            }
            else if (select.equals("change_pulse_interval")) {
                System.err.printf("> time_ms: ");
                refPulse.changeInterval(arg.nextInt());
            }
            else if (select.equals("pause_pulse")) {
                System.err.printf("> time_ms: ");
                refPulse.pausePulse(arg.nextInt());
            }
            else if (select.equals("start_pulse")) {
                refPulse.start();
            }
            else if (select.equals("stop_pulse")) {
                refPulse.stopPulse();
            }
            else if (select.equals("exit")) {
                refPulse.stopPulse();
                break;
            }
            else {
                System.err.println("There is no command '" + select + "'");
            }

            continue;
        }
    }


    public void help() {
        System.out.printf("-------------------%n");
        System.out.printf("Available commands:%n");

        System.out.printf("< change_pulse_interval (time_ms)%n");
        System.out.printf("< start_pulse%n");
        System.out.printf("< stop_pulse%n");

        System.out.printf("< exit%n");

    }


}
