import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Log{
    public final String log_filename = "log.txt";

    public static FileIO logFile;

    Log() {
        initialize();
    }

    private void initialize() {
        try {
            logFile = new FileIO(log_filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void append(String text) {
        logFile.writeToFile(String.format("%s %s", LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")), text), StandardOpenOption.APPEND);
    }
}
