import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.chart.Chart;
import javafx.scene.control.TextField;

public class guiController {
    public final Integer core_pulse_interval = 250; // refresh rate ms

    private Long start_time;
    private Pulse refPulse;
    private Log log;

    guiController() {
        initialize();
    }

    private void initialize() {
        start_time = System.currentTimeMillis();
        refPulse = new Pulse(core_pulse_interval);
        log = new Log();
    }



    @FXML
    TextField rate_value, pause_value;
    @FXML
    Chart wykres;

    @FXML
    public void onClickApplyPulseRefresh() {
        refPulse.changeInterval(Integer.valueOf(rate_value.getText()));
        System.out.printf("%s%n", rate_value.getText());
    }
    @FXML
    public void onClickApplyPauseTime() {
        refPulse.pausePulse(Integer.valueOf(pause_value.getText()));
        System.out.printf("%s%n", pause_value.getText());
    }
    @FXML
    public void onClickExit() {
        Platform.exit();
        System.exit(0);
    }
    @FXML
    public void onClickStart() {
        refPulse.start();
    }
    @FXML
    public void onClickStop() {
        refPulse.stopPulse();

    }

}
